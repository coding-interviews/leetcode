# 159. Longest Substring with At Most Two Distinct Characters

同向双指针，右指针为主指针。

**Follow up:** [Longest Substring with At Most K Distinct Characters](340.md)

```cpp
class Solution
{
public:
    int lengthOfLongestSubstringTwoDistinct(string &s)
    {
        unordered_map<char, int> counter;
        int maxlen = 0;
        for(int left = 0, right = 0; right < s.length(); right++)
        {
            counter[s[right]]++;
            while(counter.size() > 2)
            {
                counter[s[left]]--;
                if(counter[s[left]] == 0)
                    counter.erase(s[left]);
                left++;
            }
            maxlen = max(maxlen, right - left + 1);
        }
        return maxlen;
    }
};
```
