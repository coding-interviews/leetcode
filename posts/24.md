# 24. Swap Nodes in Pairs

```cpp
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution
{
public:
    ListNode* swapPairs(ListNode* head)
    {
        ListNode dummyhead(0);
        dummyhead.next = head;
        if (!head || !head->next)
            return head;
        ListNode* prev = &dummyhead, * p = head, * q = head->next, * next = q->next;
        while (true)
        {
            prev->next = q;
            q->next = p;
            p->next = next;

            if (!next || !next->next)
                break;
            prev = p;
            p = next;
            q = p->next;
            next = q->next;
        }
        return dummyhead.next;
    }
};
```
