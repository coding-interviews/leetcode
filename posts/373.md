# 373. Find K Pairs with Smallest Sums

## 暴力解法

最直接的方法就是维护一个大小不超过$`k`$的二叉堆，遍历所有元素，依次放入堆中。

时间复杂度：$`O\left((M+N)\log(M+N)\right)`$

空间复杂度：$`O(k)`$

```cpp
class Solution
{
public:
    vector<vector<int>> kSmallestPairs(vector<int>& nums1, vector<int>& nums2, int k)
    {
        multimap<int, vector<int>, greater<int>> pairs;
        for(int num1: nums1)
            for(int num2: nums2)
            {
                pairs.insert({num1 + num2, {num1, num2}});
                if(pairs.size() > k)
                    pairs.erase(pairs.begin());
            }
        vector<vector<int>> result;
        for(const pair<int, vector<int>> &p: pairs)
            result.push_back(p.second);
        return result;
    }
};
```

## 部分优化

这些数字组成的矩阵，每一行都是单调递增的。如果某一行最小的数字（第一个数字），比前一行最大的数字（最后一个数字）还大，那么后面的数字只可能更大。这样，如果二叉堆已经满了，而某一行最小的数字比堆中最大的数字还大，那后面的数字就不用考虑了。

```cpp
class Solution
{
public:
    vector<vector<int>> kSmallestPairs(vector<int>& nums1, vector<int>& nums2, int k)
    {
        if(nums2.empty())
            return {};
        multimap<int, vector<int>, greater<int>> pairs;
        for(int num1: nums1)
        {
            if(!pairs.empty() && num1 + nums2.front() >= pairs.begin()->first)
                break;
            for(int num2: nums2)
            {
                pairs.insert({num1 + num2, {num1, num2}});
                if(pairs.size() > k)
                    pairs.erase(pairs.begin());
            }
        }
        vector<vector<int>> result;
        for(const pair<int, vector<int>> &p: pairs)
            result.push_back(p.second);
        return result;
    }
};
```

## 进一步优化

观察以下矩阵：

```math
\begin{bmatrix}
1 \\ 7 \\ 11
\end{bmatrix}\oplus\begin{bmatrix}
2 \\ 4 \\ 6
\end{bmatrix}=\begin{bmatrix}
3  & 5  & 7  \\
9  & 11 & 13 \\
13 & 15 & 17
\end{bmatrix}
```

对于数字3，刚好比它大的数只能是右边的5和下边的9，其余的数字一定比5和9更大。如果我们规定了遍历的顺序只能从左上到右下，那么对于任何一个数字，我们下一个都只需要考虑它右边和下边的数字。

为了避免重复，我们规定，除第一列以外，其余数字的下一个数字只考虑右边。与前面的方法不同，我们使用最小化堆，这样每次`pop`出来的值都是最小值，这样就相当于对矩阵中的数字做了从小到大排序。

```cpp
class Solution
{
public:
    vector<vector<int>> kSmallestPairs(vector<int>& nums1, vector<int>& nums2, int k)
    {
        if(nums1.empty() || nums2.empty())
            return {};
        multimap<int, pair<int, int>> heap;
        heap.insert({nums1[0] + nums2[0], {0, 0}});
        vector<vector<int>> result;
        while(!heap.empty() && result.size() < k)
        {
            int i = heap.begin()->second.first, j = heap.begin()->second.second;
            heap.erase(heap.begin());
            result.push_back({nums1[i], nums2[j]});
            if(j + 1 < nums2.size())
                heap.insert({nums1[i] + nums2[j + 1], {i, j + 1}});
            if(j == 0 && i + 1 < nums1.size())
                heap.insert({nums1[i + 1] + nums2[j], {i + 1, j}});
        }
        return result;
    }
};
```
