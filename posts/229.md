# 229. Majority Element II

## 多数投票（Boyer-Moore Majority Vote）算法

该算法时间复杂度为$`O(N)`$，空间复杂度为$`O(1)`$，只需要对原数组进行两趟扫描，并且简单易实现。第一趟扫描我们得到两个候选值，第二趟扫描我们判断候选值出现的次数是否大于$`\left\lfloor\frac{n}{3}\right\rfloor`$。

**注意：**

- 两个候选值需要初始化成任意两个不相同的数字，如0和1。
- 候选值是出现次数最多的两个数字，但出现次数不一定超过$`\left\lfloor\frac{n}{3}\right\rfloor`$，所以需要二次判断。

```cpp
class Solution
{
public:
    vector<int> majorityElement(vector<int>& nums)
    {
        int num1 = 0, num2 = 1, count1 = 0, count2 = 0;
        for(int num: nums)
        {
            if(num == num1)
                count1++;
            else if(num == num2)
                count2++;
            else if(count1 == 0)
                num1 = num, count1 = 1;
            else if(count2 == 0)
                num2 = num, count2 = 1;
            else
                count1--, count2--;
        }

        vector<int> result;
        if(count(nums.begin(), nums.end(), num1) > nums.size() / 3)
            result.push_back(num1);
        if(count(nums.begin(), nums.end(), num2) > nums.size() / 3)
            result.push_back(num2);
        return result;
    }
};
```
