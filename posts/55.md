# 55. Jump Game

## 动态规划 (Time Limit Exceeded)

设$`f(i)`$表示能否到达下标为$`i`$的位置。对于每个位置，若$`f(i)`$为`true`，那么我们将$`f(i)`$到$`f(i + x_i)`$全部标记为`true`。这样遍历一遍后，若$`f(N - 1)`$为`true`，表明可以到达最后的位置。

**时间复杂度：** $`O(N^2)`$

**空间复杂度：** $`O(N)`$

```cpp
class Solution
{
public:
    bool canJump(vector<int>& nums)
    {
        vector<bool> reachable(nums.size(), false);
        reachable[0] = true;
        for(int i = 0; i < nums.size(); i++)
            if(reachable[i])
                for(int j = i + 1; j < min((int)nums.size(), i + nums[i] + 1); j++)
                    reachable[j] = true;
        return reachable.back();
    }
};
```

## 贪心法

此题属于典型的需要背诵的贪心法题型。我们设置一个变量`maxJump`，表示当前最远可以到达的位置。然后我们遍历整个数组，如果`nums[i]`没有超过`maxJump`，则更新`maxJump`为`max(maxjump, i + nums[i])`；否则，说明`maxJump`以后的位置均不可到达，直接返回`false`。

```cpp
class Solution
{
public:
    bool canJump(vector<int>& nums)
    {
        // 贪心法背诵题
        int maxjump = 0;
        for(int i = 0; i < nums.size(); i++)
        {
            if(maxjump < i)
                return false;
            maxjump = max(maxjump, i + nums[i]);
        }
        return maxjump >= nums.size() - 1;
    }
};
```

**时间复杂度：** $`O(N)`$

**空间复杂度：** $`O(N)`$
