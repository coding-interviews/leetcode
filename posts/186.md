# 186. Reverse Words in a String II

```cpp
class Solution
{
public:
    void reverseWords(vector<char>& s)
    {
        for(auto left = s.begin(); left < s.end(); )
        {
            auto right = find(left, s.end(), ' ');
            reverse(left, right);
            left = right + 1;
        }
        reverse(s.begin(), s.end());
    }
};
```
