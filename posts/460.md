# 460. LFU Cache

与[LRU Cache](146.md)一样，这道题同样需要两个互相耦合的数据结构，一个是key到iterator的映射（哈希表），另一个用来存储顺序信息。在[LRU Cache](146.md)中，这个顺序信息是用一个链表实现的，而这道题中，我们考虑的是每个key出现的频率，所以可以使用有序映射（即`std::map`，内部通过红黑树实现）来实现。由于频率有可能出现重复，按照题目要求，最近使用时间最远的被踢出去，所以`std::map`的value可以设置成一个链表。

为了方便，我们可以在链表的每个节点中存储所有需要的信息，即key、value和frequency。这样，我们的数据结构就变成了：

```cpp
map<int, list<Node>> order
{ 1: [2,  3,  5], 2: [7] }
      ↑   ↑   ↑       ↑
{   2:*,3:*,5:*,    7:*  }
unordered_map<int, list<Node>::iterator> cache
```

- Get操作
  - `key`不存在，直接返回-1
  - 给`key`的频率加1
    - 从`cache`中找到对应的迭代器
    - 从`order`中把这个迭代器对应的元素删掉（如果链表空了，就删除整个链表）
    - 把频率+1后的节点重新放回到`order`中
    - 更新`cache`
  - 返回`value`

- Put操作
  - `key`已存在
    - 给`key`的频率加1（同Get操作）
    - 设置`value`
  - `key`不存在
    - 若capacity已满，需要先踢出一个旧的值
      - 找到`order`中最小的频率对应的链表，这个链表的尾节点就是要踢掉的节点
      - 从这个链表中删除这个节点（如果链表空了，就删除整个链表）
      - 从`cache`中删除这个`key`
      - 更新元素数量`size`
    - 在`order`中频率为1的链表的头部插入新的节点
    - 更新`cache`
    - 更新元素数量`size`

```cpp
class LFUCache
{
private:
    struct Node
    {
        int key, value, frequency;
    };

    // capacity: 容量, size: 当前存储的元素数量
    int capacity, size;
    // key: 元素的key
    unordered_map<int, list<Node>::iterator> cache;
    // key: 频率(从小到大排列), value: 相同频率的元素集合 (head:最近访问, tail:最早访问)
    map<int, list<Node>> order;

public:
    LFUCache(int capacity)
        :capacity(capacity), size(0)
    {

    }

    void put(int key, int value)
    {
        if (capacity <= 0)
            return;
        if (cache.count(key))
        {
            refresh(key);
            cache[key]->value = value;
            return;
        }
        // 注意，先踢出旧的，再插入新的
        if (size == capacity)
        {
            list<Node>& nodes = order.begin()->second;
            int frequency = nodes.back().frequency;
            cache.erase(nodes.back().key);
            nodes.pop_back();
            if (nodes.empty())
                order.erase(frequency);
            size--;
        }
        Node n = { key, value, 1 };
        order[n.frequency].push_front(n);
        cache[key] = order[n.frequency].begin();
        size++;
    }

    int get(int key)
    {
        if (!cache.count(key))
            return -1;
        refresh(key);
        return cache[key]->value;
    }

    // 刷新key，即给key的频率加1
    void refresh(int key)
    {
        list<Node>::iterator it = cache[key];
        int value = it->value, frequency = it->frequency;
        order[frequency].erase(it);
        if (order[frequency].empty())
            order.erase(frequency);
        frequency++;
        order[frequency].push_front({ key, value, frequency });
        cache[key] = order[frequency].begin();
    }
};
```
