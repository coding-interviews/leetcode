# 97. Interleaving String

简单的矩阵型动态规划。首先，如果`s1`和`s2`长度之和与`s3`不相等，则可以直接排除。设$`f(m,n)`$表示`s1`的前$`m`$个字符和`s2`的前$`n`$个字符能否交织成`s3`的前$`m+n`$个字符。当`s3`新增加一个字符时，这个字符可能来自`s1`，也可能来自`s2`，所以就有

```math
\begin{aligned}
f(m,n) = &f(m-1, n) \text{ and } s_1[m-1] == s_3[m+n-1] \\
\text{or } &f(m, n - 1) \text { and } s_2[n-1] == s_3[m+n-1]
\end{aligned}
```

```cpp
class Solution
{
public:
    bool isInterleave(const string &s1, const string &s2, const string &s3)
    {
        if (s1.length() + s2.length() != s3.length())
            return false;
        vector<vector<bool>> dp(s1.length() + 1, vector<bool>(s2.length() + 1, false));
        dp[0][0] = true;
        for (int i = 1; i <= s1.length(); i++)
            dp[i][0] = dp[i - 1][0] && s1[i - 1] == s3[i - 1];
        for (int j = 1; j <= s2.length(); j++)
            dp[0][j] = dp[0][j - 1] && s2[j - 1] == s3[j - 1];
        for (int i = 1; i <= s1.length(); i++)
            for (int j = 1; j <= s2.length(); j++)
                dp[i][j] = s1[i - 1] == s3[i + j - 1] && dp[i - 1][j]
                || s2[j - 1] == s3[i + j - 1] && dp[i][j - 1];
        return dp.back().back();
    }
};
```
