# 89. Gray Code

```cpp
class Solution
{
public:
    vector<int> grayCode(int n)
    {
        // https://blog.csdn.net/jingfengvae/article/details/51691124
        vector<int> result(1 << n);
        for(int bin = 0; bin < 1 << n; bin++)
            result[bin] = bin ^ (bin >> 1);
        return result;
    }
};
```
