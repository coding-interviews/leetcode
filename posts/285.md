# 285. Inorder Successor in BST

```cpp
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution
{
public:
    TreeNode* inorderSuccessor(TreeNode* root, TreeNode* p)
    {
        stack<TreeNode *> s;
        while(root && root != p)
        {
            s.push(root);
            if(p->val < root->val)
                root = root->left;
            else
                root = root->right;
        }
        if(!root)
            return nullptr;
        if(root->right)
        {
            root = root->right;
            while(root->left)
                root = root->left;
            return root;
        }
        else
        {
            while(!s.empty() && s.top()->right == root)
            {
                root = s.top();
                s.pop();
            }
            return !s.empty()? s.top(): nullptr;
        }
    }
};
```
