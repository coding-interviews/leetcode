# 222. Count Complete Tree Nodes

根据完全二叉树的定义，除最后一层外，都已填满，且最后一层优先靠左填满。假设从根节点到左下角节点的距离为$`h`$，那么最后一层就会存在一个位置，其左侧的深度都是$`h`$，右侧的深度都是$`h-1`$；当然，该位置也可能不存在，即该二叉树是一棵满二叉树。那么问题来了，我们怎么找出这个节点的位置呢？当然是用二分查找法。

如下图所示，我们从根节点开始，其左子树的深度为2，右子树的深度也为2，那么未填满的节点一定在1的右子树上。节点1及其左子树的元素个数可以确定为$`2^2=4`$。

```mermaid
graph TD

1((1)) --> 2((2))
1((1)) --> 3((3))
2((2)) --> 4((4))
2((2)) --> 5((5))
3((3)) --> 6((6))
3((3)) --- null(( ))

classDef null fill:none, stroke:none
classDef lime fill:lime

class null null
class 1 lime
linkStyle 5 stroke:none
```

接下来我们处理1的右子树。3的左子树深度为1，右子树深度为0，那么未填满的节点一定在3的右子树上，以此类推，最后达到空节点。

```mermaid
graph TD

1((1)) --> 2((2))
1((1)) --> 3((3))
2((2)) --> 4((4))
2((2)) --> 5((5))
3((3)) --> 6((6))
3((3)) --- null(( ))

classDef null fill:none, stroke:none
classDef lime fill:lime
classDef gray fill:gray

class null null
class 3 lime
class 1,2,4,5 gray
linkStyle 5 stroke:none
```

计算代码如下：

```cpp
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution
{
private:
    int treeHeight(TreeNode *root)
    {
        return root? 1 + treeHeight(root->left): 0;
    }

public:
    int countNodes(TreeNode* root)
    {
        int count = 0;
        for(int h = treeHeight(root); root; h--)
            if(treeHeight(root->right) == h - 1)
            {
                count += 1 << h - 1;
                root = root->right;
            }
            else
            {
                count += 1 << h - 2;
                root = root->left;
            }
        return count;
    }
};
```

时间复杂度： $`O(h)`$

空间复杂度： $`O(h)`$
