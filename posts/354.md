# 354. Russian Doll Envelopes

## Naive 解法 (Time Limit Exceeded)

信封的长度涉及两个维度，我们无法同时比较他们，只能先比较其中一个维度，再比较另一个维度。所以，我们可以将信封按长度进行排序，对于每个信封$`(m, n)`$，依次比较长度比它小的信封，如果信封宽度也比它小，则更新套娃的数量。用数学公式表达就是：

```math
f(m, n) = \max_{0<i<m, 0<j<n} f(i, j) + 1
```

这样的时间复杂度是$`O\left(N^2\right)`$。

## 最长递增子序列 (Longest Increasing Subsequence, LIS)

将信封按其中一个维度从小到大排序后，对于另一个维度，我们需要找的是，一段递增的长度（不一定连续），这段序列的长度最长。这实际上就是最长递增子序列问题。

回顾一下[Longest Increasing Subsequence](300.md)这道题，普通的做法跟Naive解法没有本质的区别，时间复杂度同样为$`O\left(N^2\right)`$。优化的做法采用了动态规划+二分查找的方法。

```cpp
int lengthOfLIS(vector<int>& nums)
{
    vector<int> dp(nums.size(), INT_MAX);
    for(int num: nums)
        *lower_bound(dp.begin(), dp.end(), num) = num;
    return lower_bound(dp.begin(), dp.end(), INT_MAX) - dp.begin();
}
```

将其结合起来，就成了这道题的解法。需要注意的是，我们首先按横坐标从小到大排序，如果横坐标相同，需要按纵坐标从大到小排序。这是因为，用来求解最长递增子序列的数组中，我们不希望相同横坐标的数值被包含进同一个递增子序列中，最简单的方法就是把它变成递减子序列。这样就可以保证每个递增子序列中，不会包含重复的横坐标。

```cpp
class Solution
{
private:
    int lengthOfLIS(vector<int>& nums)
    {
        vector<int> dp(nums.size(), INT_MAX);
        for (int num : nums)
            *lower_bound(dp.begin(), dp.end(), num) = num;
        return lower_bound(dp.begin(), dp.end(), INT_MAX) - dp.begin();
    }

public:
    int maxEnvelopes(vector<vector<int>>& envelopes)
    {
        sort(envelopes.begin(), envelopes.end(), [](const vector<int> &p1, const vector<int> &p2)
        {
            return p1[0] < p2[0] || p1[0] == p2[0] && p1[1] > p2[1];
        });
        vector<int> nums;
        for (const vector<int>& envelope : envelopes)
            nums.push_back(envelope[1]);
        return lengthOfLIS(nums);
    }
};
```
