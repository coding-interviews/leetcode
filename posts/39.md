# 39. Combination Sum

这道题与[78.Subsets](78.md)十分相似，都是用回溯法，先尝试从`nums[i]`到`nums[n-1]`中选一个数`nums[cur]`添加到集合，然后再递归。

不同点是：

- 这道题需要记录每条DFS路径的路径和：小于`target`才可以继续递归；等于`target`则找到了一条满足要求的路径；大于`target`则路径终止。而子集问题没有这个限制，可以一直递归，直到没有元素可以选为止，同时还要把递归过程的所有路径都保存下来。
- 子集中每个元素只能被选择一次，所以递归时需要递归`dfs(cur + 1)`；而这道题每个元素可以选择任意多次，所以需要递归`dfs(cur)`。

```cpp
class Solution
{
private:
    vector<vector<int>> result;
    vector<int> candidates;
    int target;

    void dfs(vector<int>& path, int& sum, int i)
    {
        if (sum == target)
            result.push_back(path);
        else if (sum < target)
        {
            for (int next = i; next < candidates.size(); next++)
            {
                path.push_back(candidates[next]);
                sum += candidates[next];
                dfs(path, sum, next);
                sum -= candidates[next];
                path.pop_back();
            }
        }
    }

public:
    vector<vector<int>> combinationSum(vector<int>& candidates, int target)
    {
        sort(candidates.begin(), candidates.end());
        this->candidates = move(candidates);
        this->target = target;
        vector<int> path;
        int sum = 0;
        dfs(path, sum, 0);
        return result;
    }
};
```
