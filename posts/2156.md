# 2156. Find Substring With Given Hash Value

## 暴力解法 (Time Limit Exceeded)

最直接的方法就是每个位置开始，分别计算滑动窗内的哈希值，这样时间复杂度为$`O(N\cdot k)`$。根据题目的数据量$`1\le k \le N \le 2\times 10^4`$，这种做法必然会超时。

```cpp
#define c(i) (s[i] - 'a' + 1)

class Solution
{
public:
    string subStrHash(string s, int power, int modulo, int k, int hashValue)
    {
        long long hash = 0;
        for(int i = 0; i + k <= s.length(); i++)
        {
            long long hash = 0, pow = 1;
            for(int j = 0; j < k; j++)
            {
                hash = (hash + c(i + j) * pow) % modulo;
                pow = pow * power % modulo;
            }
            if(hash == hashValue)
                return s.substr(i, k);
        }

        return "";
    }
};
```

我们换个思路，既然只需要求解一个滑动窗内的和，我们能否利用部分和作差来解决呢？这样就可以把时间复杂度降低至$`O(N)`$了。这似乎是可以，设部分和$`S(i)`$为

```math
S(i) = \sum_{n=0}^{i}s_n p^n = s_0 p^0 + s_1 p^1 + \cdots + s_i p^{i-1}
```

那么

```math
S(i+k-1) - S(i-1) = s_i p^i + s_{i+1} p^{i+1} + \cdots + s_{i+k-1} p^{i+k-1}
```

而我们要求的哈希值是

```math
H(i) = s_i p^0 + s_{i+1} p^{1} + \cdots + s_{i+k-1} p^{k-1}
```

两者相差了$`p^i`$，所以我们需要再除以$`p^i`$才能得到最终的哈希值：

```math
H(i) = \frac{S(i+k-1) - S(i-1)}{p^i}
```

这样，我们可以粗略写出一段代码：

```cpp
class Solution
{
private:
    long long hash(char ch, int index, int power, int modulo)
    {
        return (ch - 'a' + 1) * (long long)pow(power, index);
    }

public:
    string subStrHash(string s, int power, int modulo, int k, int hashValue)
    {
        vector<long long> hashes(s.length());
        for(int i = 0; i < s.length(); i++)
            hashes[i] = hash(s[i], i, power, modulo);
        vector<long long> sums(s.length() + 1, 0);
        partial_sum(hashes.begin(), hashes.end(), sums.begin() + 1);
        for(int i = 0; i + k <= s.length(); i++)
        {
            int hashsum = (sums[i + k] - sums[i]) / (long long)pow(power, i) % modulo;
            if(hashsum == hashValue)
                return s.substr(i, k);
        }
        return "";
    }
};
```

但是，这样做有严重的问题。因为题目的数据量为$`1\le k \le N \le 2\times 10^4, 1\le p\le 10^9`$，如果采用上面的做法，我们可能最大需要计算到$`10^{9\times 2\times 10^4}`$，这是无论如何也不可能做到的。

## Rolling Hash + Inverse Modulo

这道题其实是经典的[滚动哈希](https://zh.wikipedia.org/wiki/%E6%97%8B%E8%BD%AC%E5%93%88%E5%B8%8C)([Rolling Hash](https://en.wikipedia.org/wiki/Rolling_hash))问题。设字符串`s[i..i+k-1]`的哈希值为$`H(i)`$，我们发现：

```math
\begin{aligned}
H(i) &= s_i p^0 + s_{i+1} p^1 + \cdots c_{i+k-1} p^{k-1} \\
H(i+1) &= \qquad\quad s_{i+1} p^0 + \cdots c_{i+k-1} p^{k-2} + c_{i-k} p^{k-1}
\end{aligned}
```

两者相差了首尾两项，以及差了$`p`$倍，即

```math
H(i+1) = \frac{H(i) - s_i p^0}{p} + c_{i-k} p^{k-1}
```

这样，我们就可以根据上述公式进行迭代。由于结果很大，上面的每一步操作都需要对`modulo`取模。在减法操作时，还需要加上`modulo`防止变成负数。公式中除以$`p`$的操作需要变成乘以分子的乘法模逆元(Modular Multiplicative Inverse)。`Python`提供了一个内置的指数函数`pow(base, exp[, mod])`，将指数设置成-1就变成了模倒数`pow(base, -1, mod)`。而C++标准库中并没有提供这样的函数（Boost提供了[`mod_inverse`](https://www.boost.org/doc/libs/1_75_0/libs/integer/doc/html/boost_integer/mod_inverse.html)函数），需要自行实现，具体实现方法可参考[这篇文章](http://conw.net/archives/6/)。

下面的代码采用了费马小定理的方法求解模倒数。需要注意的是，如果$`p`$和$`m`$的最大公约数不为1，也就是两者不互质时，逆元不存在。而题目的测试数据恰好包含了这种情况，所以下面的代码无法通过LeetCode的测试。

```cpp
#define c(i) (s[i] - 'a' + 1)

class Solution
{
private:
    vector<long long> extended_euclid_gcd(long long a, long long b) {
        // Returns a vector `result` of size 3 where:
        // Referring to the equation ax + by = gcd(a, b)
        //     result[0] is gcd(a, b)
        //     result[1] is x
        //     result[2] is y 
        
        long long s = 0; long long old_s = 1;
        long long t = 1; long long old_t = 0;
        long long r = b; long long old_r = a;

        while(r != 0) {
            long long quotient = old_r/r;
            // We are overriding the value of r, before that we store it's current
            // value in temp variable, later we assign it to old_r
            long long temp = r;
            r = old_r - quotient*r;
            old_r = temp;

            // We treat s and t in the same manner we treated r
            temp = s;
            s = old_s - quotient*s;
            old_s = temp;

            temp = t;
            t = old_t - quotient*t;
            old_t = temp;
        }
        vector<long long> result;
        result.push_back(old_r);
        result.push_back(old_s);
        result.push_back(old_t);
        return result;
    }

    long long modulo_multiplicative_inverse(long long A, long long M) {
        // Assumes that A and M are co-prime
        // Returns multiplicative modulo inverse of A under M
        
        // Find gcd using Extended Euclid's Algorithm
        vector<long long> v = extended_euclid_gcd(A, M);
        long long gcd = v[0];
        long long x = v[1];
        long long y = v[2]; // We don't really need this though

        // In case x is negative, we handle it by adding extra M
        // Because we know that multiplicative inverse of A in range M lies
        // in the range [0, M-1]
        if(x < 0) {
            x += M;
        }
        
        return x;
    }

public:
    string subStrHash(string s, int power, int modulo, int k, int hashValue)
    {
        long long hash = 0, pow = 1;
        // 计算power^(k-1)
        for(int i = 0; i < k - 1; i++)
            pow = pow * power % modulo;
        int inverse = modulo_multiplicative_inverse(power, modulo);
        for(int i = 0; i < s.length(); i++)
        {
            if(i >= k)
                hash = (modulo + hash - c(i - k)) % modulo;
            hash = (hash * inverse + c(i) * pow) % modulo;
            if(i + 1 >= k && hash == hashValue)
                return s.substr(i - k + 1, k);
        }
        return "";
    }
};
```

## Reversed Rolling Hash (Accepted)

根据上面$`H(i)`$的递推公式，我们还可以从右往左计算。很容易得到：

```math
H(i) = s_i p^0 + \left(H(i+1) - c_{i-k} p^{k-1}\right)\cdot p
```

这样就避免了求模倒数的问题。需要注意的是，题目要求返回的是the first substring，所以如果有多个子串的哈希值等于目标值，需要返回最左边的那个。

```cpp
#define c(i) (s[i] - 'a' + 1)

class Solution
{
public:
    string subStrHash(string s, int power, int modulo, int k, int hashValue)
    {
        long long hash = 0, pow = 1, result = 0;
        for(int i = s.length() - 1; i >= 0; i--)
        {
            hash = (hash * power + c(i)) % modulo;
            if(i < s.length() - k)
                hash = (modulo + hash - pow * c(i + k) % modulo) % modulo;
            else
                pow = pow * power % modulo;
            if(hash == hashValue)
                result = i;
        }
        return s.substr(result, k);
    }
};
```
