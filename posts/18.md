# 18. 4Sum

```cpp
class Solution
{
public:
    vector<vector<int>> fourSum(vector<int>& nums, int target)
    {
        if (nums.size() < 4)
            return {};
        sort(nums.begin(), nums.end());
        vector<vector<int>> result;
        for (int a = 0; a < nums.size() - 3; )
        {
            for (int b = a + 1; b < nums.size() - 2; )
            {
                for (int c = b + 1, d = nums.size() - 1; c < d; )
                {
                    int sum = nums[a] + nums[b] + nums[c] + nums[d] - target;
                    if (sum == 0)
                        result.push_back({ nums[a], nums[b], nums[c], nums[d] });
                    if (sum <= 0)
                        do
                            c++;
                        while (c < d && nums[c - 1] == nums[c]);
                    else
                        do
                            d--;
                        while (c < d && nums[d] == nums[d + 1]);
                }
                do
                    b++;
                while (b < nums.size() - 2 && nums[b - 1] == nums[b]);
            }
            do
                a++;
            while (a < nums.size() - 3 && nums[a - 1] == nums[a]);
        }
        return result;
    }
};
```
