# 490. The Maze

## 1. DFS

```cpp
class Solution
{
private:
    vector<vector<int>> matrix;
    vector<vector<bool>> visited;
    int endI, endJ;

    const pair<int, int> dir[4] = { {0,1}, {0,-1}, {-1,0}, {1,0} };

    void dfs(int i, int j)
    {
        int M = matrix.size(), N = matrix.front().size();
        if (visited[i][j])
            return;
        visited[i][j] = true;
        if (i == endI && j == endJ)
            return;
        for (const pair<int, int> &p : dir)
        {
            int x = i, y = j;
            while (x >= 0 && x < M && y >= 0 && y < N && matrix[x][y] == 0)
                x += p.first, y += p.second;
            x -= p.first, y -= p.second;
            dfs(x, y);
        }
    }

public:
    bool hasPath(vector<vector<int>>& maze, vector<int>& start, vector<int>& destination)
    {
        visited.resize(maze.size(), vector<bool>(maze.front().size(), false));
        endI = destination[0], endJ = destination[1];
        matrix = move(maze);
        dfs(start[0], start[1]);
        return visited[endI][endJ];
    }
};
```

## 2. BFS

```cpp
class Solution
{
private:
    const pair<int, int> dir[4] = { {0,1}, {0,-1}, {-1,0}, {1,0} };

    int bfs(const vector<vector<int>> &maze, int startI, int startJ, int endI, int endJ)
    {
        int M = maze.size(), N = maze.front().size();
        vector<vector<bool>> visited(M, vector<bool>(N, false));
        queue<pair<int, int>> q({ { startI, startJ } });
        visited[startI][startJ] = true;
        while (!q.empty())
        {
            int i = q.front().first, j = q.front().second;
            q.pop();
            if (i == endI && j == endJ)
                continue;
            for (const pair<int, int>& p : dir)
            {
                int x = i, y = j;
                while (x >= 0 && x < M && y >= 0 && y < N && maze[x][y] == 0)
                    x += p.first, y += p.second;
                x -= p.first, y -= p.second;
                if (!visited[x][y])
                {
                    q.push({ x, y });
                    visited[x][y] = true;
                }
            }
        }
        return visited[endI][endJ];
    }

public:
    bool hasPath(vector<vector<int>>& maze, vector<int>& start, vector<int>& destination)
    {
        return bfs(maze, start[0], start[1], destination[0], destination[1]);
    }
};
```
