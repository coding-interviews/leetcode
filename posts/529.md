# 529. Minesweeper

需要注意的地方：

- 题目要求是8邻域，不是4邻域。
- 只有当`'E'`被点击后变成`'B'`时，才会递归其邻域。

```cpp
typedef pair<int, int> Point;
const int dx[] = {-1, -1, -1, 0, 1, 1, 1, 0};
const int dy[] = {-1, 0, 1, 1, 1, 0, -1, -1};

class Solution
{
private:
    char getNext(const vector<vector<char>> &board, int i, int j)
    {
        int M = board.size(), N = board.front().size();
        if(board[i][j] == 'M')
            return 'X';
        int count = 0;
        for(int dir = 0; dir < 8; dir++)
        {
            int x = i + dx[dir], y = j + dy[dir];
            if(x >= 0 && x < M && y >= 0 && y < N)
                count += board[x][y] == 'M';
        }
        return count > 0? '0' + count: 'B';
    }

public:
    vector<vector<char>> updateBoard(vector<vector<char>>& board, vector<int>& click)
    {
        if(board.empty() || board.front().empty())
            return board;
        int M = board.size(), N = board.front().size();
        queue<Point> q({{click[0], click[1]}});
        while(!q.empty())
        {
            int i = q.front().first, j = q.front().second;
            q.pop();
            if(board[i][j] != 'E' && board[i][j] != 'M')
                continue;
            board[i][j] = getNext(board, i, j);
            for(int dir = 0; dir < 8; dir++)
            {
                int x = i + dx[dir], y = j + dy[dir];
                if(x >= 0 && x < M && y >= 0 && y < N && board[i][j] == 'B')
                    q.push({x, y});
            }
        }

        return board;
    }
};
```
