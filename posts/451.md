# 451. Sort Characters By Frequency

```cpp
class Solution
{
public:
    string frequencySort(string &s)
    {
        int counter[128] = { 0 };
        for(char ch: s)
            counter[ch]++;
        sort(s.begin(), s.end(), [&](char c1, char c2)
        {
            return counter[c1] > counter[c2] || counter[c1] == counter[c2] && c1 < c2;
        });
        return s;
    }
};
```
