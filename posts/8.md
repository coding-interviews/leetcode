# 8. String to Integer (atoi)

```cpp
class Solution
{
public:
    int myAtoi(string& str)
    {
        const char* p = str.c_str();
        // 跳过空格
        while (*p == ' ')
            p++;
        // 解析正负号
        int sign = 1;
        if (*p == '+')
            p++;
        else if (*p == '-')
            sign = -1, p++;
        // 解析数字
        long long num = 0;
        while ('0' <= *p && *p <= '9')
        {
            num = num * 10 + (*p - '0') * sign;
            if (num >= INT_MAX)
                return INT_MAX;
            else if (num <= INT_MIN)
                return INT_MIN;
            else
                p++;
        }
        return num;
    }
};
```
