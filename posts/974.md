# 974. Subarray Sums Divisible by K

既然是子数组和的问题，那么当然要用到前缀和。所以问题就变成了：

> 给定一个数组`sums`，找出满足`(sums[j] - sums[i]) % k == 0`的元素对数

最简单的做法当然是分别遍历$`i`$和$`j`$，但这样时间复杂度就会达到$`O\left(N^2\right)`$，一定会超时。

`(sums[j] - sums[i]) % k == 0`也可以理解为`sums[i] % k == sums[i] % k`，也就是要求我们在`nums[i]`右侧找出和`nums[i]`余数相同的数字。那么我们就可以从右向左遍历，同时用哈希表记录每个数字对$`k`$的余数，这样就可以把时间复杂度降至$`O(N)`$。

需要注意的是：

- 不要忘了在前缀和数组的最前面补一个0。
- 测试数据中包含负数，而负数对$`k`$取余数和正数对$`k`$取余数是不相同的，所以我们需要做一个转换。最简单的方法是加上$`k`$后再取一遍余数，即`(num % K + K) % K`。

```cpp
class Solution
{
private:
    int mod(int num, int K)
    {
        return (num % K + K) % K;
    }

public:
    int subarraysDivByK(vector<int>& A, int K)
    {
        unordered_map<int, int> remainder;
        partial_sum(A.begin(), A.end(), A.begin());
        int count = 0;
        for(int i = A.size() - 1; i >= 0; i--)
        {
            count += remainder[mod(A[i], K)];
            remainder[mod(A[i], K)]++;
        }
        count += remainder[0];
        return count;
    }
};
```
