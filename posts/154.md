# 154. Find Minimum in Rotated Sorted Array II

这道题与[前一道题](153.md)的区别是，数字可能有重复。如果重复的数字不在两端，则原答案同样适用。当最左侧和最右侧数字相同时，二分查找则无法正确排除，例如：

```
[2, 2, 2, 0, 1, 2]
```

当我们取`mid=(0+5)/2=2`时，取到的`nums[mid]`为2，这与最右侧的数字相同，我们无法确定应当排除左半部分还是右半部分。

一种简单的方法是，删除一侧相同的数字，使其变成

```
[2, 2, 2, 0, 1]
```

这样，我们就可以用[前一道题](153.md)的答案解决这道题。

```cpp
class Solution
{
public:
    int findMin(vector<int>& nums)
    {
        int left = 0, right = nums.size() - 1;
        while(left < right && nums[left] == nums[right])
            right--;
        int back = nums[right];
        while(left + 1 < right)
        {
            int mid = left + (right - left) / 2;
            if(nums[mid] > back)
                left = mid;
            else
                right = mid;
        }
        return min(nums[left], nums[right]);
    }
};
```
