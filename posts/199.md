# 199. Binary Tree Right Side View

二叉树分层遍历即可。唯一不同的是，对于每个节点，先把右子树放入队列，再把左子树放入队列。

```cpp
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution
{
public:
    vector<int> rightSideView(TreeNode* root)
    {
        if(!root)
            return {};
        queue<TreeNode *> q({root});
        vector<int> result;
        while(!q.empty())
        {
            result.push_back(q.front()->val);
            for(int i = q.size() - 1; i >= 0; i--)
            {
                TreeNode *p = q.front();
                q.pop();
                if(p->right)
                    q.push(p->right);
                if(p->left)
                    q.push(p->left);
            }
        }
        return result;
    }
};
```
