# 827. Making A Large Island

对于连通域合并的问题，我们最容易想到的方法是并查集。并查集的基本结构是：

```cpp
class DisjointSet
{
private:
    unordered_map<int, int> parent;
    int size;

public:
    DisjointSet(int size): size(size) {}

    int find(int x)
    {
        if (parent[x] == -1)
            return x;
        else
            return parent[x] = find(parent[x]);
    }

    void merge(int x, int y)
    {
        int root1 = find(x), root2 = find(y);
        if (root1 != root2)
            parent[root2] = root1;
    }
};
```

在这道题中，我们还需要提供一个功能，返回某个连通域的面积。所以，我们需要在类中添加一个类别到面积的映射，在合并连通域时，将被合并的连通域的面积清零，并添加到新的连通域里：

```cpp
void merge(int x, int y)
{
    int root1 = find(x), root2 = find(y);
    if (root1 != root2)
        parent[root2] = root1,
        area[root1] += area[root2],
        area[root2] = 0;
}
```

基本思路就是：把所有为1的点加入到不相交集里，并记下此时的最大面积。然后依次在每一个空白的地方尝试添加1，计算合并后的最大连通域面积。需要注意的是，这里的合并并不一定真的要合并，否则我们处理下个点时，还要把刚刚尝试合并的集合重新解开，这是很难做到的。实际上，我们只需要将这个点的四个邻域所在连通域的面积相加就可以了（不过四个邻域不一定在都在不同的连通域里，注意去重）。

```cpp
class Solution
{
private:
    class DisjointSet
    {
    private:
        int parent[500 * 500], area[500 *500];
        int size, max_area = 0;

    public:
        DisjointSet(int size): size(size) {}

        void insert(int index)
        {
            parent[index] = -1;
            area[index] = 1;
            max_area = max(max_area, area[index]);
        }

        int find(int x)
        {
            if (parent[x] == -1)
                return x;
            else
                return parent[x] = find(parent[x]);
        }

        void merge(int x, int y)
        {
            int root1 = find(x), root2 = find(y);
            if (root1 != root2)
                parent[root2] = root1,
                area[root1] += area[root2],
                area[root2] = 0,
                max_area = max(max_area, area[root1]);
        }

        int get_area(int x)
        {
            return area[x];
        }

        int largest_area() const
        {
            return max_area;
        }
    };

public:
    int largestIsland(vector<vector<int>>& grid)
    {
        constexpr int dx[4] = { -1, 0, 0, 1 };
        constexpr int dy[4] = { 0, -1, 1, 0 };
        const int M = grid.size(), N = grid.front().size();
        DisjointSet ds(M * N);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                if (grid[i][j])
                    ds.insert(i * N + j);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                if (grid[i][j])
                    for (int dir = 0; dir < 4; dir++)
                    {
                        int next_i = i + dx[dir], next_j = j + dy[dir];
                        if (0 <= next_i && next_i < M && 0 <= next_j && next_j < N && grid[next_i][next_j])
                            ds.merge(i * N + j, next_i * N + next_j);
                    }

        int max_area = ds.largest_area();
        for(int i = 0; i < M; i++)
            for(int j = 0; j < N; j++)
                if (grid[i][j] == 0)
                {
                    int area = 1;
                    unordered_set<int> visited;
                    for (int dir = 0; dir < 4; dir++)
                    {
                        int next_i = i + dx[dir], next_j = j + dy[dir];
                        if (0 <= next_i && next_i < M && 0 <= next_j && next_j < N &&grid[next_i][next_j])
                        {
                            int root = ds.find(next_i * N + next_j);
                            if(visited.count(root))
                                continue;
                            visited.insert(root);
                            area += ds.get_area(root);
                        }
                    }
                    max_area = max(max_area, area);
                }
        return max_area;
    }
};
```
