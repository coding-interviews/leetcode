# 308. Range Sum Query 2D - Mutable

```cpp
class NumMatrix
{
private:
    vector<vector<int>> matrix, bit;

    int lowbit(int x) const
    {
        return x & (-x);
    }

    int prefixSum(int row, int col) const
    {
        int sum = 0;
        for(int i = row + 1; i >= 1; i -= lowbit(i))
            for(int j = col + 1; j >= 1; j -= lowbit(j))
                sum += bit[i][j];
        return sum;
    }

public:
    NumMatrix(const vector<vector<int>> &matrix)
    {
        if(matrix.empty() || matrix.front().empty())
            return;
        int M = matrix.size(), N = matrix.front().size();
        this->matrix.resize(M, vector<int>(N, 0));
        this->bit.resize(M + 1, vector<int>(N + 1, 0));
        for(int i = 0; i < M; i++)
            for(int j = 0; j < N; j++)
                update(i, j, matrix[i][j]);
    }

    void update(int row, int col, int val)
    {
        int M = matrix.size(), N = matrix.front().size();
        int delta = val - matrix[row][col];
        matrix[row][col] = val;
        for(int i = row + 1; i <= M; i += lowbit(i))
            for(int j = col + 1; j <= N; j += lowbit(j))
                bit[i][j] += delta;
    }

    int sumRegion(int row1, int col1, int row2, int col2)
    {
        return prefixSum(row2, col2) - prefixSum(row1 - 1, col2)
            - prefixSum(row2, col1 - 1) + prefixSum(row1 - 1, col1 - 1);
    }
};

/**
 * Your NumMatrix object will be instantiated and called as such:
 * NumMatrix obj = new NumMatrix(matrix);
 * obj.update(row,col,val);
 * int param_2 = obj.sumRegion(row1,col1,row2,col2);
 */
```
