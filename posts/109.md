# 109. Convert Sorted List to Binary Search Tree

链表不像数组，不能直接取中点，但我们可以事先记录一下中点的位置。

```cpp
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution
{
private:
    int length(ListNode *head)
    {
        int result = 0;
        while(head)
        {
            result++;
            head = head->next;
        }
        return result;
    }

    TreeNode *sortedListToBST(ListNode * &head, int n)
    {
        if(n == 0)
            return nullptr;
        else
        {
            TreeNode *root = new TreeNode(0);
            root->left = sortedListToBST(head, n / 2);
            root->val = head->val;
            head = head->next;
            root->right = sortedListToBST(head, n - n / 2 - 1);
            return root;
        }
    }

public:
    TreeNode* sortedListToBST(ListNode* head)
    {
        int n = length(head);
        return sortedListToBST(head, n);
    }
};
```
