# 129. Sum Root to Leaf Numbers

```cpp
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution
{
private:
    int total = 0;

    void sumNumbers(TreeNode *root, int cursum)
    {
        cursum = cursum * 10 + root->val;
        if(!root->left && !root->right)
        {
            total += cursum;
            return;
        }
        if(root->left)
            sumNumbers(root->left, cursum);
        if(root->right)
            sumNumbers(root->right, cursum);
    }

public:
    int sumNumbers(TreeNode* root)
    {
        if(!root)
            return 0;
        sumNumbers(root, 0);
        return total;
    }
};
```
