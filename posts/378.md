# 378. Kth Smallest Element in a Sorted Matrix

## 大小为$`N`$的最小化堆

我们先看一道类似的题目：[N数组第K大元素](https://www.lintcode.com/problem/543)

> 在N个数组中找到第K大元素

在[我的题解](https://www.lintcode.com/problem/543/note/249547)中详细解释了那道题的三种解法。而在LeetCode的这道题中，由于每一行已经是排序的了，很显然，第三种方法适用于这道题，因为省去了排序的步骤，时间复杂度为$`O(k\log N)`$。

```cpp
class Solution
{
public:
    int kthSmallest(vector<vector<int>>& matrix, int k)
    {
        priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> heap;
        vector<int> indices(matrix.size(), 1);
        for(int i = 0; i < matrix.size(); i++)
            heap.push({matrix[i].front(), i});

        for(int i = 0; i < k - 1; i++)
        {
            auto [value, index] = heap.top();
            heap.pop();
            if(indices[index] < matrix[index].size())
                heap.push({matrix[index][indices[index]++], index});
        }
        return heap.top().first;
    }
};
```

## 二分猜答案法

矩阵的左上角和右下角元素分别是最小值和最大值，假设答案为`mid`，验证一下`mid`是偏大还是偏小。具体验证方法是统计所有小于或等于`mid`元素的个数，如果数量小于$`k`$，则`mid`偏小，反之则**可能**偏大。

二分猜答案的过程时间复杂度为$`O\left(\log(\max-\min)\right)`$，对于每次查找，我们需要统计小于或等于`mid`的元素个数，这个过程需要对每一行调用`upper_bound`函数，时间复杂度为$`O(N\log N)`$，所以总的时间复杂度为$`O\left(N\log N\log(\max-\min)\right)`$。

```cpp
class Solution
{
private:
    int numSmallerOrEqual(vector<vector<int>> &matrix, int num)
    {
        int count = 0;
        for(const vector<int> &row: matrix)
            count += upper_bound(row.begin(), row.end(), num) - row.begin();
        return count;
    }

public:
    int kthSmallest(vector<vector<int>>& matrix, int k)
    {
        int left = matrix.front().front(), right = matrix.back().back();
        while(left + 1 < right)
        {
            int mid = left + (right - left) / 2;
            if(numSmallerOrEqual(matrix, mid) < k)
                left = mid;
            else
                right = mid;
        }
        if(numSmallerOrEqual(matrix, left) >= k)
            return left;
        else
            return right;
    }
};
```

## 从左上到右下查找

由于每一行和每一列都以升序排列，所以对于任何一个位置，右下方的所有元素都应该比它大。基于这个思想，我们就可以按照这个顺序进行查找。

定义一个最小化堆，从左上角的元素开始，把它放入堆中。左上角的元素一定是最小的元素，而第二小的元素要么是它右边的元素，要么是它下面的元素。因此把堆顶元素（也就是最小的元素）弹出以后，将右边和下面的元素都放入堆中，那么此时新的堆顶就是第二小的元素了。按照这个顺序，我们就可以依次访问最小、第二小、第三小的元素，第$`k`$次取出的元素就是第$`k`$小的元素。

这样，我们一共需要push $`2k`$次，pop $`k`$次，而堆的大小从1均匀地增加到了$`k`$，所以平均时间复杂度应当为$`O(k\log k)`$。很明显，这种方法不依赖于数组大小$`N`$，我们每一步操作都是最优解。

需要注意的是，一个元素不能重复入堆，需要记录。

```cpp
class Solution
{
public:
    int kthSmallest(vector<vector<int>>& matrix, int k)
    {
        vector<vector<bool>> visited(matrix.size(), vector<bool>(matrix.size(), false));
        typedef tuple<int, int, int> Point;
        priority_queue<Point, vector<Point>, greater<Point>> heap;
        heap.push({ matrix[0][0], 0, 0 });
        for (int i = 0; i < k - 1; i++)
        {
            auto [value, x, y] = heap.top();
            heap.pop();
            if (x < matrix.size() - 1 && !visited[x + 1][y])
            {
                heap.push({ matrix[x + 1][y], x + 1, y, });
                visited[x + 1][y] = true;
            }
            if (y < matrix.size() - 1 && !visited[x][y + 1])
            {
                heap.push({ matrix[x][y + 1], x, y + 1 });
                visited[x][y + 1] = true;
            }
        }
        auto [value, x, y] = heap.top();
        return value;
    }
};
```
