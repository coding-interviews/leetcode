# 124. Binary Tree Maximum Path Sum

分治法。设`f(root)`表示以`root`为根节点的Path Sum的最大值，那么我们分别求出`f(root->left)`和`f(root->right)`，则`f(root)`为`max(left, 0) + root->val`和`max(right, 0) + root->val`中较大的一个。

```cpp
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution
{
private:
    int maxsum = INT_MIN;
    int dfs(TreeNode *root)
    {
        if(!root)
            return 0;
        int left = max(0, dfs(root->left));
        int right = max(0, dfs(root->right));
        maxsum = max(maxsum, left + root->val + right);
        return root->val + max(left, right);
    }

public:
    int maxPathSum(TreeNode* root)
    {
        dfs(root);
        return maxsum;
    }
};
```
