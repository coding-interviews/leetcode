# 115. Distinct Subsequences

## 双序列型动态规划

与[Interleaving String](97.md)类似，这也是一道典型的双序列型动态规划题目。设$`f(m, n)`$表示`s`的前$`m`$个字符和`t`的前$`n`$个字符能组成的Distinct Subsequences数量。

当我们给`s`添加一个字符时，就有两种情况：

- `s[-1] != t[-1]`，此时这个新的字符没有任何卵用，序列的数量跟不添加这个字符一样，即$`f(m-1,n)`$。
- `s[-1] == t[-1]`，如果我们选择不将`s[-1]`和`t[-1]`匹配，那么序列数量就跟上面的情况相同，也是$`f(m-1,n)`$；如果我们选择将他们匹配，那么`t`就被用掉了最后一个字符，得到的序列数量就应当是$`f(m-1,n-1)`$。最终的结果应当是这两者相加，即$`f(m-1,n) + f(n-1,n-1)`$。

总结一下，就是：

```math
f(m,n) = \begin{cases}
f(m-1, n)               & s_m \ne t_n \\
f(m-1, n) + f(m-1, n-1) & s_m = t_n
\end{cases}
```

最后再考虑一下初始条件，当$`n=0`$时，不同的序列数量始终为1，这个从Leetcode的测试结果上也能看出。当$`m=0`$且$`n>0`$时，由于$`s`$的字符小于$`t`$的字符数量，不可能匹配成功，所以结果始终为0。

```math
\begin{cases}
f(m, 0) = 1 & m \ge 0 \\
f(0, n) = 0 & n > 0
\end{cases}
```

```cpp
class Solution
{
public:
    int numDistinct(const string &s, const string &t)
    {
        vector<vector<unsigned int>> dp(s.length() + 1, vector<unsigned int>(t.length() + 1, 0));
        for(int i = 0; i <= s.length(); i++)
            dp[i][0] = 1;
        for(int i = 1; i <= s.length(); i++)
            for(int j = 1; j <= t.length(); j++)
                if(s[i - 1] == t[j - 1])
                    dp[i][j] = dp[i - 1][j] + dp[i - 1][j - 1];
                else
                    dp[i][j] = dp[i - 1][j];
        return dp.back().back();
    }
};
```

## 滚动数组优化

我们注意到，$`f(m,n)`$只与$`f(m-1,n)`$和$`f(m-1,n-1)`$有关，所以只需要一个一维数组就够了。由于$`f(m,n)`$依赖于左上方和上方的点，所以我们需要从右到左计算。

```cpp
class Solution
{
public:
    int numDistinct(const string &s, const string &t)
    {
        vector<unsigned int> dp(t.length() + 1, 0);
        dp[0] = 1;
        for(int i = 1; i <= s.length(); i++)
            for(int j = t.length(); j > 0; j--)
                if(s[i - 1] == t[j - 1])
                    dp[j] += dp[j - 1];
        return dp.back();
    }
};
```
