# 135. Candy

## 优先级队列

每个人至少有一块糖果。然后我们需要根据每个人和他左右两个人的rating来调整糖果数量。为了避免调整的过程中相互影响，最简单的方法是根据rating从小到大来调整，也就需要一个最小化堆来控制顺序。

```cpp
class Solution
{
private:
    struct Compare
    {
        bool operator()(const pair<int, int> &p1, const pair<int, int> &p2) const
        {
            return p1.second > p2.second;
        }
    };

public:
    int candy(vector<int>& ratings)
    {
        vector<int> candies(ratings.size(), 1);
        // key: index, value: rate
        priority_queue<pair<int, int>, vector<pair<int, int>>, Compare> heap;
        for(int i = 0; i < ratings.size(); i++)
            heap.push({i, ratings[i]});
        while(!heap.empty())
        {
            int i = heap.top().first;
            heap.pop();
            if(i > 0 && ratings[i - 1] < ratings[i])
                candies[i] = max(candies[i], candies[i - 1] + 1);
            if(i < ratings.size() - 1 && ratings[i] > ratings[i + 1])
                candies[i] = max(candies[i], candies[i + 1] + 1);
        }
        return accumulate(candies.begin(), candies.end(), 0);
    }
};
```

## 双向遍历

另一种避免互相依赖的方法是，从左到右和从右到左各遍历一次。

```cpp
class Solution
{
public:
    int candy(vector<int>& ratings)
    {
        vector<int> candies(ratings.size(), 1);
        for(int i = 1; i < ratings.size(); i++)
            if(ratings[i - 1] < ratings[i])
                candies[i] = max(candies[i], candies[i - 1] + 1);
        for(int i = ratings.size() - 2; i >= 0; i--)
            if(ratings[i] > ratings[i + 1])
                candies[i] = max(candies[i], candies[i + 1] + 1);
        return accumulate(candies.begin(), candies.end(), 0);
    }
};
```
