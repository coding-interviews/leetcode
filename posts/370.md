# 370. Range Addition

Hint说，每次只需要更新第一个和最后一个元素即可。但这个解法不是那么容易想到，需要一点技巧。

假设我们的数组范围是`[0,n)`，需要更新的区间是`[start, end]`，更新值是`inc`，那么将区间`[start, end]`中每个数字加上`inc`，等同于将区间`[start, n)`内的数字都加上`inc`，然后将`[end+1, n)`区间内数字都减去 `inc`。这样，我们的数组就变成了

```
0 0 Δ 0 0 0 0 -Δ 0 0 0
    ↑       ↑
  start    end
```

而真正的结果应该是

```
0 0 Δ Δ Δ Δ Δ 0 0 0 0
    ↑       ↑
  start    end
```

那么如何将左侧的`inc`传递到右侧的每一个元素上面呢？很简单，只需要对数组求部分和即可。

```cpp
class Solution
{
public:
    vector<int> getModifiedArray(int length, vector<vector<int>>& updates)
    {
        vector<int> result(length + 1, 0);
        for(const vector<int> &update: updates)
            result[update[0]] += update[2], result[update[1] + 1] -= update[2];
        partial_sum(result.begin(), result.end(), result.begin());
        result.pop_back();
        return result;
    }
};
```
