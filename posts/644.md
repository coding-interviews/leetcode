# 644. Maximum Average Subarray II

和[Maximum Average Subarray I](https://leetcode.com/problems/maximum-average-subarray-i)不同，这道题要求子数组的长度$`\ge k`$，所以我们就不能用滑动窗来解决，否则我们需要分别遍历长度为$`k,k+1,\cdots,N`$的滑动窗。

我们也不能使用[Maximum Subarray](53.md)的做法，因为有一个子数组长度的限制。

这道题的正确做法是二分猜答案。我们猜测长度$`\ge k`$的子数组最大平均值为$`T`$，然后去验证这样的$`T`$是否存在，即是否存在$`p,q`$使得：

```math
\begin{cases}
\frac{x_p+x_{p+1}+\cdots+x_q}{q-p+1} \ge T & \textcircled{1} \\
q - p + 1 \ge k & \textcircled{2}
\end{cases}
```

对$`\textcircled{1}`$式两边同时乘以$`q-p+1`$，可得

```math
x_p+x_{p+1}+\cdots+x_q \ge (q-p+1)T
```

再进一步移项，可以得到

```math
(x_p - T) + (x_{p+1} - T) + \cdots + (x_q - T) \ge 0
```

如果我们令$`y_i = x_i - T`$，那么原问题就转化成了：能否找到$`p,q`$，使得

```math
\begin{cases}
y_p + y_{p+1} + \cdots + y_q \ge 0 \\
q - p + 1 \ge k
\end{cases}
```

这样，我们需要分别记录$`0\sim i`$的前缀和，以及$`0\sim i - k`$前缀和的最大值。当遍历到$`y_i`$时，如果

```math
S_i - \max_i S_{i-k} \ge 0
```

则说明这样的子数组存在，其中$`S_i = y_1 + y_2 + \cdots + y_i`$。

```cpp
class Solution
{
private:
    bool subarrayExist(vector<int> &nums, double avg, int k)
    {
        double sum_left = 0, sum_right = 0, sum_left_min = 0;
        for(int i = 0; i < nums.size(); i++)
        {
            sum_right += nums[i]- avg;
            if(i >= k)
                sum_left += nums[i - k] - avg, sum_left_min = min(sum_left_min, sum_left);
            if(i >= k - 1 && sum_right - sum_left_min >= 0)
                return true;
        }
        return false;
    }

public:
    double findMaxAverage(vector<int>& nums, int k)
    {
        double left = *min_element(nums.begin(), nums.end());
        double right = *max_element(nums.begin(), nums.end());
        while(left + 1e-5 < right)
        {
            double mid = left + (right - left) / 2;
            if(subarrayExist(nums, mid, k))
                left = mid;
            else
                right = mid;
        }
        return left;
    }
};
```
