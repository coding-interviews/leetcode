# 987. Vertical Order Traversal of a Binary Tree

本题需要注意的地方：X坐标相同时，按Y坐标顺序输出；当Y坐标也相同时，按从小到大顺序输出。

所以，本题需要分层层次遍历，对每一层相同X坐标的数据进行排序。

```cpp
/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
class Solution
{
private:
    map<int, vector<int>> values;

public:
    vector<vector<int>> verticalTraversal(TreeNode* root)
    {
        if(!root)
            return {};
        queue<pair<TreeNode *, int>> q({{root, 0}});
        while(!q.empty())
        {
            unordered_map<int, vector<int>> level;
            for(int i = q.size() - 1; i >= 0; i--)
            {
                pair<TreeNode *, int> p = q.front();
                q.pop();
                level[p.second].push_back(p.first->val);
                if(p.first->left)
                    q.push({p.first->left, p.second - 1});
                if(p.first->right)
                    q.push({p.first->right, p.second + 1});
            }
            for(const pair<int, vector<int>> &p: level)
            {
                vector<int> level_values = p.second;
                sort(level_values.begin(), level_values.end());
                for(int item: level_values)
                    values[p.first].push_back(item);
            }
        }

        vector<vector<int>> result;
        for(const pair<int, vector<int>> &p: values)
            result.push_back(p.second);
        return result;
    }
};
```
