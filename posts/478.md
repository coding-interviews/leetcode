# 478. Generate Random Point in a Circle

```cpp
class Solution
{
private:
    default_random_engine generator;
    uniform_real_distribution<double> distribution;

    const double radius, x_center, y_center;

public:
    Solution(double radius, double x_center, double y_center)
    : distribution(-radius, radius), radius(radius), x_center(x_center), y_center(y_center)
    {}

    vector<double> randPoint()
    {
        double x, y;
        do
        {
            x = distribution(generator);
            y = distribution(generator);
        } while(x * x + y * y > radius * radius);
        return { x + x_center, y + y_center };
    }
};

/**
 * Your Solution object will be instantiated and called as such:
 * Solution* obj = new Solution(radius, x_center, y_center);
 * vector<double> param_1 = obj->randPoint();
 */
```
