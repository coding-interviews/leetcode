.PHONY: build
build: SUMMARY.md gitbook
	npx gitbook build

.ONESHELL:
SUMMARY.md:
	echo "# Summary" > $@
	for filename in $$(ls -v posts/*.md); do
		title=$$(awk "NR == 1" $$filename)
		title=$${title#"# "}
		echo "- [$$title]($$filename)" >> $@
	done

.PHONY: gitbook
gitbook:
	npm install gitbook-cli gitbook-plugin-katex-gitlab
	npx gitbook install

.PHONY: clean
clean:
	$(RM) -r SUMMARY.md node_modules package-lock.json public
